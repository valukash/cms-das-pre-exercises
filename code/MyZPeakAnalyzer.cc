#include <map>
#include <string>

#include "TH1.h"

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ParameterSet/interface/ParameterSetDescription.h"
#include "FWCore/ParameterSet/interface/ConfigurationDescriptions.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "DataFormats/PatCandidates/interface/Muon.h"

#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"

class MyZPeakAnalyzer : public edm::one::EDAnalyzer<> {
public:
  explicit MyZPeakAnalyzer(const edm::ParameterSet&);
  ~MyZPeakAnalyzer() override;
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  void beginJob() override;
  void analyze(const edm::Event&, const edm::EventSetup&) override;
  void endJob() override;

  // simple map to contain all histograms;
  // histograms are booked in the beginJob()
  // method
  std::map<std::string, TH1F*> histContainer_;
  // ----------member data ---------------------------
  edm::EDGetTokenT<pat::MuonCollection> muonCollToken_;
  edm::EDGetTokenT<pat::ElectronCollection> elecCollToken_;
};

MyZPeakAnalyzer::MyZPeakAnalyzer(const edm::ParameterSet& iConfig)
    : histContainer_(),
      muonCollToken_(consumes<pat::MuonCollection>(iConfig.getUntrackedParameter<edm::InputTag>("muonSrc"))),
      elecCollToken_(consumes<pat::ElectronCollection>(iConfig.getUntrackedParameter<edm::InputTag>("elecSrc"))) {}

MyZPeakAnalyzer::~MyZPeakAnalyzer() {}

void MyZPeakAnalyzer::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  edm::ParameterSetDescription desc;
  desc.addUntracked<edm::InputTag>("muonSrc", edm::InputTag("slimmedMuons"));
  desc.addUntracked<edm::InputTag>("elecSrc", edm::InputTag("slimmedElectrons"));
  descriptions.add("analyzeBasicPat", desc);
}

void MyZPeakAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {
  // get pat muon collection
  const auto& muons = iEvent.get(muonCollToken_);

  // fill pat muon histograms
  for (auto mu1 = muons.cbegin(); mu1 != muons.cend(); ++mu1) {
    histContainer_["muonPt"]->Fill(mu1->pt());
    histContainer_["muonEta"]->Fill(mu1->eta());
    histContainer_["muonPhi"]->Fill(mu1->phi());

    if (mu1->pt() > 20 && std::abs(mu1->eta()) < 2.1) {
      for (auto mu2 = muons.cbegin(); mu2 != muons.cend(); ++mu2) {
        if (mu2 > mu1) {
          // check only muon pairs of unequal charge
          if (mu1->charge() != mu2->charge()) {
            if (mu2->pt() > 20 && std::abs(mu2->eta()) < 2.1) {
              histContainer_["mumuMass"]->Fill((mu1->p4() + mu2->p4()).mass());
            }
          }
        }
      }
    }
  }

  // get pat electron collection
  const auto& electrons = iEvent.get(elecCollToken_);

  // loop over electrons
  for (const auto& electron : electrons) {
    histContainer_["elePt"]->Fill(electron.pt());
    histContainer_["eleEta"]->Fill(electron.eta());
    histContainer_["elePhi"]->Fill(electron.phi());
  }

  // Multiplicity
  histContainer_["eleMult"]->Fill(electrons.size());
  histContainer_["muonMult"]->Fill(muons.size());
}

void MyZPeakAnalyzer::beginJob() {
  // register to the TFileService
  edm::Service<TFileService> fs;

  histContainer_["mumuMass"] = fs->make<TH1F>("mumuMass", "mass", 90, 30., 120.);

  // book histograms for Multiplicity:
  histContainer_["eleMult"] = fs->make<TH1F>("eleMult", "electron multiplicity", 100, 0, 50);
  histContainer_["muonMult"] = fs->make<TH1F>("muonMult", "muon multiplicity", 100, 0, 50);

  // book histograms for Pt:
  histContainer_["elePt"] = fs->make<TH1F>("elePt", "electron Pt", 100, 0, 200);
  histContainer_["muonPt"] = fs->make<TH1F>("muonPt", "muon Pt", 100, 0, 200);

  // book histograms for Eta:
  histContainer_["eleEta"] = fs->make<TH1F>("eleEta", "electron Eta", 100, -5, 5);
  histContainer_["muonEta"] = fs->make<TH1F>("muonEta", "muon Eta", 100, -5, 5);

  // book histograms for Phi:
  histContainer_["elePhi"] = fs->make<TH1F>("elePhi", "electron Phi", 100, -3.5, 3.5);
  histContainer_["muonPhi"] = fs->make<TH1F>("muonPhi", "muon Phi", 100, -3.5, 3.5);
}

void MyZPeakAnalyzer::endJob() {}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(MyZPeakAnalyzer);
